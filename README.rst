=====
SmartShopppingList
=====

SmartShopppingList is a simple Django app manage your Shopping List.

Quick start
-----------

1. Add "SmartShopppingList" to your INSTALLED_APPS setting like this::

    INSTALLED_APPS = [
        ...
        'SmartShopppingList',
    ]

2. Include the SmartShopppingList URLconf in your project urls.py like this::

    path('SmartShopppingList/', include('SmartShopppingList.urls')),

3. Run `python manage.py migrate` to create the SmartShopppingList models.

4. Start the development server and visit http://127.0.0.1:8000/admin/
   to create a item (you'll need the Admin app enabled).

5. Visit http://127.0.0.1:8000/SmartShopppingList/ to manage items in the list.