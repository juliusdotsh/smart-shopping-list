from django import forms
from .models import item

class itemForm(forms.ModelForm):
    class Meta:
        model = item
        fields = ['name','quantity']
        widgets = {
            'name': forms.TextInput(attrs={'class': 'input'}),
            'quantity' : forms.TextInput(attrs={'class': 'input','type': 'range'})
        }