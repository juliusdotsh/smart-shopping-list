from django.db import models

class item(models.Model):
    name = models.CharField(max_length=200)
    quantity = models.PositiveIntegerField(default=1)
