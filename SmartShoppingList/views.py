from io import StringIO
from django.shortcuts import render, get_list_or_404, redirect
from django.http import HttpResponse
from .models import item
from .ModelForm import itemForm
import json

def index(request):
    if request.method == 'POST':
        form = itemForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('smartShoppingList:index')
    form = itemForm()
    items = item.objects.all()
    return render(request, 'index.html', {'items': items, 'form' : form})

def deleteItem(request, primaryKey):
    item.objects.get(pk=primaryKey).delete()
    result = "success"
    return HttpResponse(json.dumps(result))
