from django.apps import AppConfig


class SmartshoppinglistConfig(AppConfig):
    name = 'SmartShoppingList'
