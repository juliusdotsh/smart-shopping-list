function deleteItem(itemPrimaryKey){
    $.ajax({
        url:'deleteItem/'+itemPrimaryKey,
        success: function(data){
        data = eval(data)
            if(data == 'success'){
                location.reload();
            }else{
                console.error("There was an error deleting the item from the list:" + data);
            }
        },    
        error: function(xhr){
            alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
        }
    });
}