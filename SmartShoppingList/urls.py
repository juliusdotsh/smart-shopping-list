from django.urls import path
from . import views

app_name="smartShoppingList"

urlpatterns = [
    path('', views.index, name="index"),
    path('deleteItem/<int:primaryKey>', views.deleteItem, name="deleteItem"),
]
